from django import forms

class ProjectID(forms.Form):
    project_id = forms.IntegerField()