from django.urls import path
from . import views

urlpatterns = [
    
    path('index/',views.Index.as_view(),name= 'index'),
    path('',views.Home.as_view(), name= 'home'),
    path('branches/<int:project_id>/<str:access_token>/', views.Branches.as_view(), name='branches'),
    path('direct/',views.Direct.as_view(),name= 'direct'),
]