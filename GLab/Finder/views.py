from re import template
import os
from datetime import datetime
from urllib import request
from django.shortcuts import redirect, render
from django.http import HttpResponse
from .forms import ProjectID
from django.views.generic import TemplateView, FormView, View
import requests
import json
from dotenv import load_dotenv
load_dotenv()

# Create your views here.
class Home(View):
    def get(self,request):
        return render(request,'Finder/index.html')

class Direct(View):
    def get(self,request):
        callback_url = 'http://127.0.0.1:8000/index'
        scope = 'api+read_api+read_repository'
        app_id = os.getenv("app_id")
        secret = os.getenv("secret")
        auth_code_url = "https://gitlab.com/oauth/authorize?client_id={}&redirect_uri={}&response_type=code&state={}&scope={}".format(app_id,callback_url,secret,scope)
        return redirect(auth_code_url)
   
class Index(View):
    def get(self,request):
        return render(request,'Finder/base.html')

    def post(self,request):
        auth_code = request.GET['code']
        callback_url = 'http://127.0.0.1:8000/index'
        scope = 'api+read_api+read_repository'
        app_id = os.getenv("app_id")
        secret = os.getenv("secret")
        response = requests.post("https://gitlab.com/oauth/token?client_id={}&client_secret={}&code={}&grant_type=authorization_code&redirect_uri={}".format(app_id,secret,auth_code,callback_url))
        access_token = response.json()['access_token']
        project_id = request.POST['projectid']
        return redirect('branches',project_id,access_token)

class Branches(View):
    def get(self,request,project_id,access_token):
        """refresh_token = response.json()['refresh_token']
        response1 = requests.post('https://gitlab.example.com/oauth/token?client_id={}&client_secret={}&refresh_token={}&grant_type=refresh_token&redirect_uri={}'.format(app_id,secret,refresh_token,callback_url))
        access_token = response1.json()['access_token']"""
        branches_url = "https://gitlab.com/api/v4//projects/{}/repository/branches?access_token={}&pagination=keyset&per_page=100".format(project_id,access_token)
        response_data = requests.get(branches_url)
        
        if response_data.status_code == 200:
            data = json.loads(response_data.text)
            #protected_branches_url = "https://gitlab.com/api/v4//projects/{}/repository/protected_branches".format(pk)
            #releases_url = "https://gitlab.com/api/v4/projects/{}/releases?pagination=keyset&per_page=100".format(pk)
            #releases_rawdata = requests.get(releases_url)
            #releases_data = json.loads(releases_rawdata.text)
            tags_url = "https://gitlab.com/api/v4/projects/{}/repository/tags?access_token={}&pagination=keyset&per_page=100".format(project_id,access_token)
            tags_rawdata = requests.get(tags_url)
            tags_data = json.loads(tags_rawdata.text)
            
            
            branch_list = []
            for i in range(len(data)):
                branch_list.append(data[i]['name'])
            """for i in range(len(releases_data)):
                branch_list.append(releases_data[i]['name'])"""
            for i in range(len(tags_data)):
                branch_list.append(tags_data[i]['name'])
            #branch_updated = list(set(branch_list))
            return render(request,'Finder/branches.html',{'branches':branch_list})
        else:
            return render(request,'Finder/error.html')

    def post(self,request,project_id,access_token):
        branch1_name = request.POST['dropdown1']
        branch2_name = request.POST['dropdown2']

        branch1_url = "https://gitlab.com/api/v4/projects/{}/repository/commits?ref_name={}&access_token={}&pagination=keyset&per_page=100".format(project_id,branch1_name,access_token)
        branch1_commit_raw = requests.get(branch1_url)
        #print(branch1_commit_raw.headers['Link'])
        branch1_commit_data = json.loads(branch1_commit_raw.text)
        
        branch2_url = "https://gitlab.com/api/v4/projects/{}/repository/commits?ref_name={}&access_token={}&pagination=keyset&per_page=100".format(project_id,branch2_name,access_token)
        branch2_commit_raw = requests.get(branch2_url)
        branch2_commit_data = json.loads(branch2_commit_raw.text)

        branch1_commits = {}
        branch2_commits = {}
        """for i in range(len(branch1_commit_data)):
            #branch1_commits.append(branch1_commit_data[i]['id'])
             branch1_commits[branch1_commit_data[i]["short_id"]] = [branch1_commit_data[i]["id"],branch1_commit_data[i]["short_id"],branch1_commit_data[i]["author_name"],branch1_commit_data[i]["message"],branch1_commit_data[i]["authored_date"],branch1_commit_data[i]["committed_date"]]
        for i in range(len(branch2_commit_data)):
            #branch2_commits.append(branch2_commit_data[i]['id'])
            branch2_commits[branch2_commit_data[i]["short_id"]] = [branch2_commit_data[i]["id"],branch2_commit_data[i]["short_id"],branch2_commit_data[i]["author_name"],branch2_commit_data[i]["message"],branch2_commit_data[i]["authored_date"],branch2_commit_data[i]["committed_date"]]"""
        for i in range(len(branch1_commit_data)):
            #branch1_commits.append({'id':branch1_commit_data[i]["id"],'short_id':branch1_commit_data[i]["short_id"],'author':branch1_commit_data[i]["author_name"],'message':branch1_commit_data[i]["message"],'authored_date':branch1_commit_data[i]["authored_date"],'committed_date':branch1_commit_data[i]["committed_date"]})
            branch1_commits[branch1_commit_data[i]["short_id"]] = [branch1_commit_data[i]["id"],branch1_commit_data[i]["short_id"],branch1_commit_data[i]["author_name"],branch1_commit_data[i]["message"],branch1_commit_data[i]["authored_date"],branch1_commit_data[i]["committed_date"]]
        #print(len(branch1_commit_data))
        #print(len(branch2_commit_data))
        for i in range(len(branch2_commit_data)):
            #branch2_commits.append({'id':branch2_commit_data[i]["id"],'short_id':branch2_commit_data[i]["short_id"],'author':branch2_commit_data[i]["author_name"],'message':branch2_commit_data[i]["message"],'authored_date':branch2_commit_data[i]["authored_date"],'committed_date':branch2_commit_data[i]["committed_date"]})
            branch2_commits[branch2_commit_data[i]["short_id"]] = [branch2_commit_data[i]["id"],branch2_commit_data[i]["short_id"],branch2_commit_data[i]["author_name"],branch2_commit_data[i]["message"],branch2_commit_data[i]["authored_date"],branch2_commit_data[i]["committed_date"]]

        diff = list(set(branch1_commits)-set(branch2_commits))
        commits = []                                                                                                                                                    #branch1_commits[i][4]                                      
        for i in diff:
            #z = branch1_commits[i][4].split('+')
            commits.append({'id':branch1_commits[i][0],'short_id':branch1_commits[i][1],'author':branch1_commits[i][2],'message':branch1_commits[i][3],'authored_date':datetime.strptime(branch1_commits[i][4],"%Y-%m-%dT%H:%M:%S.%f%z").replace(tzinfo=None),'committed_date':branch1_commits[i][5]})
            
        #sorted(commits, key = lambda i: i['committed_date'],reverse=True)
        
        return render(request,'Finder/commits.html',{'commits':sorted(commits, key = lambda i: i['committed_date'],reverse=True),'branch1':branch1_name,'branch2':branch2_name})       
    

